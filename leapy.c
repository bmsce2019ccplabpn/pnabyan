#include <stdio.h>

int input(int a)
{
    printf("please enter a year:");
    scanf("%d",&a);
    return a;
}

int compute(int year)
{
	if(year%100==0 && year%400==0)
        return 2;
    else if(year%4==0)
            return 1;
        else 
            return 0;
}

void output(int r)
{
	if(r==1 || r==2)
        printf("it's a leap year\n");
    else 
        printf("it's not a leap year\n");
}

int main()
{
    int a,year,r;
    year=input(a);
    r=compute(year);
    output(r);
	return 0;
}
