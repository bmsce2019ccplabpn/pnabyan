#include <stdio.h>

int input()
{
    int a;
    printf("please enter your result:");
    scanf("%d",&a);
    return a;
}
char compute(int a)
{
    char gd;
    if(a>=85) 
        gd='A';
    else if(a<85&&a>=75)
        gd='B'; 
    else if(a<75&&a>=65)
        gd='C';
    else if(a<65&&a>=55)
        gd='D';
    else gd='E';
    return gd;
}
void output(char gd)
{
    
    printf("your grade is %c\n",gd);
}

int main()
{
    int a;
    char gd;
    a=input();
    gd=compute(a);
    output(gd);
    return 0;
}