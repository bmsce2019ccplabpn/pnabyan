#include <stdio.h>
int input()
{
    int x;
    printf("enter a number:");
    scanf("%d",&x);
    return x;
}

int compute(int a)
{
    return a*a;
}

void output(int a,int sqr)
{
    printf("square of %d=%d\n",a,sqr);
}

int main()
{
	int a,sqr;
    a=input();
	sqr = compute(a);
    output(a,sqr);
    return 0;
}