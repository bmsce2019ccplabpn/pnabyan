#include<stdio.h>
#include<math.h>

int main()
{
    int x1,y1,x2,y2;
    float distance;
    printf("enter x and y coordinate of the first point:");
    scanf("%d%d",&x1,&y1);
    printf("enter x and y coordinate of the second point:");
    scanf("%d%d",&x2,&y2);
    distance=sqrt(pow((x2-x1),2)+ pow((y2-y1),2));
    printf("distance between the two points is %.3f\n",(float)distance);

    return 0;
}


