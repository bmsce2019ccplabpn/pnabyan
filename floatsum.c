#include <stdio.h>
float compute(float,float);
void input(float *a,float *b);
void output(float,float,float);
int main()
{
	float a,b,sum;
    input(&a,&b);
	sum = compute(a,b);
    output(a,b,sum);
    return 0;
}

void input(float *a,float *b)
{
    printf("enter two decimals:");
    scanf("%f%f",a,b);
}
float compute(float a,float b)
{
    float res;
    res=(float)a+b;
    return res;
}

void output(float a,float b,float sum)
{
    printf("%.2f+%.2f=%.2f\n",a,b,sum);
}