#include<stdio.h>
#include<math.h>
struct coordinate{int x1,y1,x2,y2;};

void input()
{
    struct coordinate points;
    printf("enter x and y coordinate of the first point:");
    scanf("%d%d",&points.x1,&points.y1);
    printf("enter x and y coordinate of the second point:");
    scanf("%d%d",&points.x2,&points.y2);
}

float compute(struct coordinate points)
{
    float dist;
    dist=sqrt(pow((points.x2-points.x1),2)+ pow((points.y2-points.y1),2));
    return dist;
}

void output(float distance)
{
    printf("distance between the two points is %.3f\n",(float)distance);
}
int main()
{
    struct coordinate points;
    float distance;
    input( );
    printf("%d%d",points.x1,points.y1);
    distance=compute(points);
    output(distance);
    return 0;
}

