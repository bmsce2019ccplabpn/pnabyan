#include<stdio.h>
#include<math.h>

struct coordinate{int x1,y1,x2,y2;};

struct coordinate input()
{
    struct coordinate p;
    printf("enter x and y coordinate of the first point:");
    scanf("%d%d",&p.x1,&p.y1);
    printf("enter x and y coordinate of the second point:");
    scanf("%d%d",&p.x2,&p.y2);
    return p;
}

float compute(struct coordinate points)
{
    float dist;
    dist=sqrt(pow((points.x2-points.x1),2)+ pow((points.y2-points.y1),2));
    return dist;
}

void output(float distance)
{
    printf("distance between the two points is %.3f\n",(float)distance);
}
int main()
{
    struct coordinate points;
    float distance;
    points = input();
    distance=compute(points);
    output(distance);
    return 0;
}
