#include <stdio.h>
int fact(int i)
{
    int t=1,s=1;
    for(t=1;t<=i;t++)
        s*=t;
    return s;
}
int main()
{
    int i=1,n;
    float k,h=0.0;
    printf("enter a number:");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
        {k=(float) i/fact(i);
        h+=k;}
    printf("%.3f is the sum of factorials of the given number\n",(float)h);
    return 0;
}
