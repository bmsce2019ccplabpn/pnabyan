#include<stdio.h>
int input()
{
    int x;
    printf("enter a number:");
    scanf("%d",&x);
    return x;
}

int compute(int a,int b)
{
    int sum;
    sum=a+b;
    return sum;
}

void output(int a,int b,int res)
{
    printf("%d+%d=%d\n",a,b,res);
}

int main()
{
    int a,b,res;
    a=input();
    b=input();
    res=compute(a,b);
    output(a,b,res);
    return 0;
}
