#include <stdio.h>
int main()
{
    int n,i=0,small,large,positionA=1,positionB=1;
    printf("how many numbers you want to enter:");
    scanf("%d",&n);
    int nums[n];
    for(i=0;i<n;i++)
    {
        printf("%d value: ",i+1);
        scanf("%d",&nums[i]);
    }

    printf("\nlist of the arrays\n");
    for(i=0;i<n;i++)
    {
        printf("%d number=%d\n",i+1,nums[i]);
    }

    small=nums[0];
    large=nums[0];
    for(i=0;i<n;i++)
    {
        if(nums[i]<small)
            {  small=nums[i];
               positionA=i+1;   }

        if(nums[i]>large)
            {  large=nums[i];
               positionB=i+1;   }
    }

    printf("smallest of all=%d and position=%d\n",small,positionA);
    printf("largest of all=%d and position=%d\n",large,positionB);

    nums[positionB-1]=small;
    nums[positionA-1]=large;

    printf("\nafter interchanging the numbers\n");
    printf("list of the arrays\n");
    for(i=0;i<n;i++)
    {
        printf("%d number=%d\n",i+1,nums[i]);
    }

    return 0;
}
