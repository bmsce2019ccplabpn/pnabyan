#include <stdio.h>

void input(int *a,int *b, int *c)
{
    printf("please enter three numbers:\n");
    scanf("%d%d%d",a,b,c);
}

int compute(int *a,int *b,int *c)
{
   if(*a>*b && *a>*c)
      return 1;
   else if(*b>*a && *b>*c) 
      return 2;
      else
      return 0;
}

void output(int *a,int *b,int *c,int large)
{
    if(large==1)
    printf(" %d is greatest among all",*a);
    else if(large==2)
    printf(" %d is greatest among all",*b);
    else
    printf(" %d is greatest among all",*c);
}

int main()
{
    int a,b,c,large;
    input(&a,&b,&c);
    large=compute(&a,&b,&c);
    output(&a,&b,&c,large);
    return 0;
}