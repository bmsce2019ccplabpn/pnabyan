#include <stdio.h>

int main()
{
   int n1, n2, sum, subst,mult;
   float div,prcnt12,prcnt21;
   printf("please enter two numbers\n");
   scanf("%d%d", &n1, &n2);

   sum = n1 + n2 ;
   subst = n1 - n2 ;
   mult = n1 * n2 ;
   div =(float) n1/n2;
   prcnt12=(float) n1*100/n2;
   prcnt21=(float) n2*100/n1;

   printf("sum=%d\n",sum);
   printf("subtraction=%d\n",subst);
   printf("multiplication=%d\n",mult);
   printf("division=%.2f\n",div);
  if (n1<n2)
  {
        printf("percent of N1 out of N2=%.2f",prcnt12);
  }
  else
  {
        printf("percent of N2 out of N1=%.2f%\n",prcnt21);
  }
    return 0;
}