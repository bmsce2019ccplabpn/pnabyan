#include<stdio.h>
#include<math.h>

void input(int *x1,int *x2,int *y1,int *y2)
{
    printf("enter x and y coordinate of the first point:");
    scanf("%d%d",x1,y1);
    printf("enter x and y coordinate of the second point:");
    scanf("%d%d",x2,y2);
}

float compute(int x1,int x2,int y1,int y2)
{
    float dist;
    dist=sqrt(pow((x2-x1),2)+ pow((y2-y1),2));
    return dist;
}

void output(float distance)
{
    printf("distance between the two points is %.3f\n",(float)distance);
}
int main()
{
    int x1,y1,x2,y2;
    float distance;
    input(&x1,&x2,&y1,&y2);
    distance=compute(x1,x2,y1,y2);
    output(distance);
    return 0;
}

