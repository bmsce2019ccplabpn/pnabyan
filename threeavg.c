#include <stdio.h>

void input(int *a,int *b,int *c)
{
    printf("please enter three numbers:");
    scanf("%d%d%d",a,b,c);
}

void compute(int a,int b,int c,int *total,float *avg,int *larg,int *small)
{
    *total=a+b+c;
    *avg=(float)((a+b+c)/3);
    
    if(a>b&&a>c) 
        *larg=a;
    else if(b>a&&b>c) 
        *larg=b;
         else *larg=c;

    if(a<b&&a<c)
        *small=a;
    else if(b<a&&b<c) 
        *small=b;
         else *small=c;
}
void output(int total,float avg,int larg,int small)
{
	printf("total=%d \naverage=%.2f \nlarge=%d \nsmall=%d \n",total,avg,larg,small);
}

int main()
{
    int a,b,c,total,larg,small;
    float avg;
    input(&a,&b,&c);
    compute(a,b,c,&total,&avg,&larg,&small);
    output(total,avg,larg,small);
	return 0;
}
