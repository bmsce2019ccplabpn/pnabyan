#include<stdio.h>
float input()
{
    float x;
    printf("enter a fraction number:");
    scanf("%f",&x);
    return x;
}

float compute(float a,float b)
{
    float sum;
    sum=a+b;
    return sum;
}

void output(float a,float b,float res)
{
    printf("%f+%f=%f\n",a,b,res);
}

int main()
{
    float a,b,res;
    a=input();
    b=input();
    res=compute(a,b);
    output(a,b,res);
    return 0;
}