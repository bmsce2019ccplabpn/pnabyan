#include<stdio.h>
int input()
{
    int x;
    printf("enter the number of fractions you want to add:");
    scanf("%d",&x);
    return x;
}

float compute(int n)
{
    int i=1;
    float t,sum=0.0;
    while(i<=n)
      {
        printf("%d number:",i);
        scanf("%f"&t);
        sum=sum+t;
        i++;
      }
    return sum;
}

void output(int n,float res)
{
    printf("the sum of the given %d fractions=%f\n",n,(float)res);
}

int main()
{
    int n;
    float res;
    n=input();
    res=compute(n);
    output(n,res);
    return 0;
}
