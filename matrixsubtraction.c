#include <stdio.h>
int main()
{
    int r1,c1,r2,c2,i=0,j=0;
    printf("enter row and column of the first matrix:");
    scanf("%d%d",&r1,&c1);
    printf("enter row and column of the second matrix:");
    scanf("%d%d",&r2,&c2);

    if(r1!=r2&&c1!=c2)
    {
        printf("operation can't be done\n");
        exit(0);
    }

    int m2[r2][c2];
    int m1[r1][c1];
    int m3[r1][c1];
    printf("enter values of first matrix\n");
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
            {
             scanf("%d",&m1[i][j]);
            }
    }
    printf("enter values of the second matrix\n");
    for(i=0;i<r2;i++)
    {
        for(j=0;j<c2;j++)
            {
             scanf("%d",&m2[i][j]);
            }
    }
    for(i=0;i<r2;i++)
    {
        for(j=0;j<c2;j++)
           {
               m3[i][j]=m1[i][j]-m2[i][j];
           }
    }
    printf("\nsubtraction between first matrice and second matrice\n");
    for(i=0;i<r2;i++)
    {
        for(j=0;j<c2;j++)
           {
               printf("  %d  ",m3[i][j]);
           }
        printf("\n");
    }

    return 0;
}